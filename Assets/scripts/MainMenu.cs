using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    [SerializeField]
    private Slider _slider;

    [SerializeField]
    private TMP_Dropdown _dropdown;

    public void StartGame()
    {
        PlayerPrefs.SetInt("PlayerAmount", (int)_slider.value);
        PlayerPrefs.SetInt("QuestionList", _dropdown.value);
        SceneManager.LoadScene(1);
    }
}
