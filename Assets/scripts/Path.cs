using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class Path : MonoBehaviour
{
    public List<Transform> Positions;

    /*
    [SerializeField]
    private LineRenderer _defaultLineRenderer;

    [SerializeField]
    private Material _ladderMaterial;
    */

    public static Path Instance;

    private void Start()
    {
        Instance = this;
    }

    public async Task<int> HandlePlayerLanding(int pos)
    {

        int num = pos + 1;
        int? primePosition = getPrimePosition(num);
        if (!primePosition.HasValue)
        {
            return 0;
        }
        if (primePosition % 5 == 0)
        {
            return 3;
        }
        if (primePosition % 3 == 0 && num != 5)
        {
            return -4;
        }

        return ( await UIManager.Instance.AskQuestion(
                QuestionManager.Instance.GetRandomQuestion()
                )
            ) ? 3 : 0;
    }

    private int? getPrimePosition(int number)
    {
        if (!isPrime(number))
        {
            return null;
        }
        int num = 0;
        for (int i = 2; i < number; i++)
        {
            if (isPrime(number))
            {
                num++;
            }
        }
        return num;
    }

    private bool isPrime(int number)
    {
        if (number <= -1)
        {
            return false;
        }
        for (int i = 2; i < number; i++)
        {
            if (number % i == 0)
            {
                return false;
            }
        }
        return true;
    }
}
