using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    private GameObject _winEffect;

    public static GameManager Instance;

    public int PlayerAmount;

    public List<Player> PlayerList;

    private int _currentPlayer;

    private void Start()
    {
        Instance = this;
        PlayerAmount = PlayerPrefs.GetInt("PlayerAmount");
    }

    public void Win(Player playerWinner)
    {
        Object.Instantiate(_winEffect, new Vector3(0f, -5f, 0f), Quaternion.identity);
        StartCoroutine(EndGame());
    }

    public void HandleDiceRoll(int value)
    {
        PlayerList[_currentPlayer].Advance(value);
        _currentPlayer++;
        if (_currentPlayer >= PlayerList.Count)
        {
            _currentPlayer = 0;
        }
    }

    private IEnumerator EndGame()
    {
        yield return new WaitForSeconds(10f);
        Application.Quit();
    }
}