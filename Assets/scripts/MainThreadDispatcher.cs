using System;
using System.Collections.Concurrent;
using UnityEngine;

public class MainThreadDispatcher : MonoBehaviour
{
    private static readonly ConcurrentQueue<Action> _actions = new ConcurrentQueue<Action>();

    public static void Enqueue(Action action)
    {
        _actions.Enqueue(action);
    }

    private void Update()
    {
        Action result;
        while (_actions.TryDequeue(out result))
        {
            result?.Invoke();
        }
    }
}
