using System;
using System.Collections.Generic;
using UnityEngine;

public class QuestionManager : MonoBehaviour
{
    [SerializeField]
    private List<Question> _questions;

    [SerializeField]
    private List<Question> _unityQuestions;

    [SerializeField]
    private List<Question> _generalQuestions;

    private int questionType;

    private List<Question> _selectedQuestions;

    public static QuestionManager Instance;

    public Question GetRandomQuestion()
    {
        int num = new System.Random().Next(0, _selectedQuestions.Count - 1);
        return _selectedQuestions[num];
    }

    private void Start()
    {
        Instance = this;
        questionType = PlayerPrefs.GetInt("QuestionList");
        switch (questionType)
        {
            case 0:
                _selectedQuestions = _questions;
                break;
            case 1:
                _selectedQuestions = _unityQuestions;
                break;
            case 2:
                _selectedQuestions = _generalQuestions;
                break;
        }
    }
}
