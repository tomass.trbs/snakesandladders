using System.Collections.Generic;
using UnityEngine;

public class Spawn : MonoBehaviour
{
    [SerializeField]
    private GameObject _playerModel;

    [SerializeField]
    private Transform _spawnPosition;

    [SerializeField]
    private List<Sprite> _listSprite;

    private void Start()
    {
        for (int i = 0; i < GameManager.Instance.PlayerAmount; i++)
        {
            Player component = Object.Instantiate(_playerModel, _spawnPosition.position, Quaternion.identity).GetComponent<Player>();
            GameManager.Instance.PlayerList.Add(component);
            component.GetComponent<SpriteRenderer>().sprite = _listSprite[i];
            component.Advance(0);
        }
    }
}
