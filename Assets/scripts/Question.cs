using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="MyQuestion", menuName = "Question")]
public class Question : ScriptableObject
{
    public int Id;
    public string QuestionTitle;
    public string QuestionText;
    public string[] QuestionOptions;
    public int QuestionAnswer;
}
