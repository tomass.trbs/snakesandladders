using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public static UIManager Instance;

    [Header("Usual stuff")]
    [SerializeField]
    private Image _diceOutput;

    [SerializeField]
    private List<Sprite> _diceFaces = new List<Sprite>();

    [SerializeField]
    private Button _button;

    [Space(10f)]
    [Header("Question thing")]
    [SerializeField]
    private Canvas _questionUI;

    [SerializeField]
    private GameObject _questionPanel;

    [SerializeField]
    private Sprite _buttonSprite;

    // [SerializeField]
    // private Sprite _checkmarkSprite;

    private int _correctAnswer = -1;

    private int _selectedOption = -2;

    private TaskCompletionSource<bool> _tcs;

    private void Start()
    {
        Instance = this;
        _questionUI.enabled = false;
        _questionPanel.SetActive(value: false);
    }

    public int RollDice()
    {
        int num = UnityEngine.Random.Range(1, 7);
        StartCoroutine(ShuffleDice(num));
        return num;
    }

    public void ShowDiceValue(int value)
    {
        _diceOutput.sprite = _diceFaces[value - 1];
    }

    private IEnumerator ShuffleDice(int finalValue)
    {
        ShowDiceValue(UnityEngine.Random.Range(1, 7));
        yield return new WaitForSeconds(0.5f);
        ShowDiceValue(UnityEngine.Random.Range(1, 7));
        yield return new WaitForSeconds(0.5f);
        ShowDiceValue(UnityEngine.Random.Range(1, 7));
        yield return new WaitForSeconds(0.5f);
        ShowDiceValue(finalValue);
    }

    public void ClickButton()
    {
        StartCoroutine(SpamProtection());
        GameManager.Instance.HandleDiceRoll(RollDice());
    }

    private IEnumerator SpamProtection()
    {
        _button.interactable = false;
        yield return new WaitForSeconds(2f);
        _button.interactable = true;
    }

    public async Task<bool> AskQuestion(Question question)
    {
        try
        {
            MainThreadDispatcher.Enqueue(delegate
            {
                Debug.Log("test");
                GenerateQuestionGUI(question);
                _questionUI.enabled = true;
                _questionPanel.SetActive(value: true);
            });
            _correctAnswer = question.QuestionAnswer;
            _tcs = new TaskCompletionSource<bool>();
            MainThreadDispatcher.Enqueue(delegate
            {
                StartCoroutine(ManageQuestion());
            });
            bool result = await _tcs.Task;
            MainThreadDispatcher.Enqueue(delegate
            {
                _questionUI.enabled = false;
                _questionPanel.SetActive(value: false);
            });
            return result;
        }
        catch (Exception ex)
        {
            Debug.LogError("Error in AskQuestion: " + ex.Message);
            return false;
        }
        finally
        {
            _correctAnswer = -1;
        }
    }

    private IEnumerator ManageQuestion()
    {
        yield return new WaitForSeconds(50f);
        if (!_tcs.Task.IsCompleted)
        {
            _tcs.TrySetResult(result: false);
        }
    }

    private void OnButtonClicked(ToggleGroup toggleGroup)
    {
        try
        {
            StopCoroutine(ManageQuestion());
            bool result = _selectedOption == _correctAnswer;
            _tcs.TrySetResult(result);
        }
        catch (Exception ex)
        {
            Debug.LogError("Error in OnButtonClicked: " + ex.Message);
            _tcs.TrySetResult(result: false);
        }
    }

    private void GenerateQuestionGUI(Question question)
    {
        foreach (Transform item in _questionPanel.transform)
        {
            UnityEngine.Object.Destroy(item.gameObject);
        }
        TextMeshProUGUI component = new GameObject("QuestionText", typeof(TextMeshProUGUI)).GetComponent<TextMeshProUGUI>();
        component.text = question.QuestionText;
        component.color = Color.black;
        component.enableAutoSizing = true;
        component.fontSizeMin = 10f;
        component.fontSizeMax = 30f;
        component.alignment = TextAlignmentOptions.Center;
        component.transform.SetParent(_questionPanel.transform, worldPositionStays: false);
        ToggleGroup toggleGroup = new GameObject("ToggleGroup", typeof(RectTransform), typeof(ToggleGroup), typeof(VerticalLayoutGroup)).GetComponent<ToggleGroup>();
        toggleGroup.transform.SetParent(_questionPanel.transform, worldPositionStays: false);
        float fontSizeMax = Mathf.Min(20, 60 / question.QuestionOptions.Length);
        int num = 0;
        string[] questionOptions = question.QuestionOptions;
        foreach (string text in questionOptions)
        {
            int currentCounter = num;
            Toggle component2 = new GameObject("Toggle", typeof(RectTransform), typeof(Toggle)).GetComponent<Toggle>();
            component2.GetComponent<Toggle>().group = toggleGroup;
            component2.transform.SetParent(toggleGroup.transform, worldPositionStays: false);
            TextMeshProUGUI toggleText = new GameObject("ToggleText", typeof(TextMeshProUGUI)).GetComponent<TextMeshProUGUI>();
            toggleText.text = text;
            toggleText.color = Color.black;
            toggleText.enableAutoSizing = true;
            toggleText.fontSizeMin = 10f;
            toggleText.fontSizeMax = fontSizeMax;
            toggleText.alignment = TextAlignmentOptions.Left;
            toggleText.transform.SetParent(component2.transform, worldPositionStays: false);
            component2.onValueChanged.AddListener(delegate (bool value)
            {
                toggleText.fontStyle = (value ? FontStyles.Bold : FontStyles.Normal);
            });
            component2.onValueChanged.AddListener(delegate (bool value)
            {
                if (value)
                {
                    _selectedOption = currentCounter;
                }
            });
            num++;
        }
        GameObject obj = new GameObject("SubmitButton", typeof(RectTransform), typeof(Button), typeof(Image));
        obj.GetComponent<Image>().sprite = _buttonSprite;
        Button component3 = obj.GetComponent<Button>();
        TextMeshProUGUI component4 = new GameObject("submitBtnText", typeof(TextMeshProUGUI)).GetComponent<TextMeshProUGUI>();
        component4.text = "Submit";
        component4.color = Color.black;
        component4.enableAutoSizing = true;
        component4.fontSizeMin = 10f;
        component4.fontSizeMax = 25f;
        component4.alignment = TextAlignmentOptions.Center;
        component4.transform.SetParent(component3.transform, worldPositionStays: false);
        component3.onClick.AddListener(delegate
        {
            OnButtonClicked(toggleGroup);
        });
        component3.transform.SetParent(_questionPanel.transform, worldPositionStays: false);
    }
}