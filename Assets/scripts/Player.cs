using System.Collections;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;

public class Player : MonoBehaviour
{
    public int posInPath = -1;

    private Vector3 _newPosition;

    [SerializeField]
    private float _speed;

    private float _timer;

    public void Advance(int dist, bool handleLanding = true)
    {
        int potentialNewPos = posInPath + dist;

        if (potentialNewPos <= 0)
        {
            _newPosition = Path.Instance.Positions[0].position;
            posInPath = 0;
        }

        else if (potentialNewPos >= Path.Instance.Positions.Count)
        {
            if (potentialNewPos == Path.Instance.Positions.Count)
            {
                _newPosition = Path.Instance.Positions[Path.Instance.Positions.Count - 1].position;
                posInPath = Path.Instance.Positions.Count - 1;
                GameManager.Instance.Win(this);
            } else {
                Advance(-(posInPath + dist - Path.Instance.Positions.Count));
            }
        }
        else if (GameManager.Instance.PlayerList.Any((Player player) => player != this && player.posInPath == potentialNewPos))
        {
            posInPath += dist;
            Advance(1);
        }
        else
        {
            _newPosition = Path.Instance.Positions[potentialNewPos].position;
            posInPath = potentialNewPos;
            if (handleLanding)
            {
                StartCoroutine(AdvanceAfterTime());
            }
            _timer = 0f;
        }
    }

    private void Update()
    {
        _timer += Time.deltaTime * _speed;
        if (base.transform.position != _newPosition)
        {
            base.transform.position = Vector3.Lerp(base.transform.position, _newPosition, _timer);
            if (Vector3.Distance(base.transform.position, _newPosition) < 0.01f)
            {
                base.transform.position = _newPosition;
                _timer = 0f;
            }
        }
    }

    private IEnumerator AdvanceAfterTime()
    {
        Task<int> t = Task.Run(async () => await Path.Instance.HandlePlayerLanding(posInPath));
        yield return new WaitForSeconds(1f);
        yield return new WaitUntil(() => t.IsCompleted);
        Advance(t.Result, handleLanding: false);
    }
}